import urllib3
from bs4 import BeautifulSoup

def get_upcoming_events (url):

    req = urllib3.PoolManager()
    res = req.request('GET',url)

    soup = BeautifulSoup(res.data, 'html.parser')

    events = soup.find('ul', {'class':'list-recent-events'}).findAll('li')

    for event in events:
        events_details = dict()
        events_details['name'] = event.find('h3').find("a").text
        events_details['location'] = event.find('span',{'class':'event-location'}).text
        events_details['time'] = event.find('time').text
        print(events_details)

get_upcoming_events('https://www.python.org/events/python-events/')
